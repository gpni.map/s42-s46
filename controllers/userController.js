const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Order = require('../models/Order');

//User Registration Function
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})

	return User.find({email: newUser.email}).then (result => {
		if(result.length > 0) {
			return "Email exisitng"
		} else {
			return newUser.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		}
	});
	
};

//User Login Function
module.exports.userLogin = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

//Retrieve ALL Users
module.exports.retrieveAllUser = (userData, idParams) => {
	return User.find({}).then(result=>{
		if(!userData.isAdmin){
			return "Not an admin"
		} else {
			return result
		}
	})
};

//Retrieve User
module.exports.retrieveUser = (userData, idParams) => {
	return User.findById(userData.id).then(result => {
		if(userData.id == idParams.userId) {
			return result
		} else {
	 		return false
		}
	})	
};


//Ser user as admin
module.exports.setAsAdmin = (userData, idParams, reqBody) => {
	if(userData.isAdmin) {
		let setToAdmin = {
			isAdmin: reqBody.isAdmin
		}

		return User.findByIdAndUpdate(idParams.userId, setToAdmin).then((setToAdmin, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	} 
};



