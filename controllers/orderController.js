const Order = require('../models/Order');
const auth = require('../auth')
const User = require('../models/User')
const Product = require('../models/Product');


//Create Order
module.exports.createOrder = async (userData, reqBody) => {
		
	if(userData.isAdmin){
		return 'admin cannot order'
	} else {
		function getAmount(value1, value2) {
			if(value2 === undefined){
				value2 = 1;
			}
			let myAmount = value1 * value2
			return myAmount
		}


		let result = await Product.findById(reqBody.productId).populate("price", {path: "price"});
		let newOrder = new Order({
			userId: userData.id,
			products: [{
				productId: reqBody.productId,
				quantity: reqBody.quantity
			}],
			totalAmount: getAmount(result.price, reqBody.quantity)
		})

		return newOrder.save().then((data, error) => {
			if (error) {
				return false
			} else {
				return 'Order successful'
			}
		})
	}
};



//Retrieve all orders
module.exports.getAllOrders = (userData) => {
	if(userData.isAdmin) {
		return Order.find({}).then(result => {
			return result
		})
	} else {
		return false
	}
};


//Retrieve User Order
module.exports.retrieveUserOrder = async (userData, idParams) => {
	if(userData.id == idParams.userId) {
		let user = await User.findById(idParams.userId);
		let myOrder = await Order.find({userId: user});
		return myOrder;
	} else {
		return false
	}
};
