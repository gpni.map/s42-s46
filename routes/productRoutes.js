const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


//Add Product (ADMIN)
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
});


//Retrieve ALL Products (ADMIN)
router.get('/all', auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization)

	productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
})



//Retrieve All 'ACTIVE' Products
router.get("/", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
});


//Retrieve Single Product
router.get("/:productId", (req, res) => {

	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController))
});


//Update Product Information (ADMIN)
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
});



//Archive Product (ADMIN)
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.archiveProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))

});




module.exports = router;



